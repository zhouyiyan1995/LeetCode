package LongestSubstring;

import java.util.HashSet;
import java.util.Set;

public class LongesetSubstring {
	public int lengthOfLongestSubstring(String s) {
		int maximum = 0;
		char[] character = new char[100];
		int count = 0;
		int iter = 0;
		boolean exist = false;
		for (int i = 0; i < s.length(); i++) {
			for (int j = 0; j < count; j++) {
				if (character[j] == s.charAt(i)) {
					exist = true;
				}
			}
			if (exist == false) {
				character[count] = s.charAt(i);
				count++;
				if (count > maximum) {
					maximum = count;
				}
			}
			else {
				if (count > maximum) {
					maximum = count;
				}
				count = 1;
				character = new char[100];
				iter++;
				i = iter;
				character[0] = s.charAt(i);
				exist = false;
			}
		}
		return maximum;
	}
	
	public int slideWindow(String s) {
		int n = s.length();
        Set<Character> set = new HashSet<>();
        int ans = 0, i = 0, j = 0;
        while (i < n && j < n) {
            // try to extend the range [i, j]
            if (!set.contains(s.charAt(j))){
                set.add(s.charAt(j++));
                ans = Math.max(ans, j - i);
            }
            else {
                set.remove(s.charAt(i++));
            }
        }
        return ans;
	}
}
