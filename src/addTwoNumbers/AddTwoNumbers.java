package addTwoNumbers;

public class AddTwoNumbers {
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode result = new ListNode(0);
		ListNode e = result;
		int carry = 0;
		int digit = 0;
		while (l1 != null || l2 != null) {
			if (l1 != null && l2 != null) {
				digit = l1.val + l2.val + carry;
				if (digit >= 10) {
					digit = digit - 10;
					carry = 1;
				} else {
					carry = 0;
				}
			}
			if (l1 != null && l2 == null) {
				digit = l1.val + carry;
				if (digit >= 10) {
					digit = digit - 10;
					carry = 1;
				} else {
					carry = 0;
				}
			}
			if (l1 == null && l2 != null) {
				digit = l2.val + carry;
				if (digit >= 10) {
					digit = digit - 10;
					carry = 1;
				} else {
					carry = 0;
				}
			}
			e.val = digit;
			if (l1 != null) {
				l1 = l1.next;
			}
			if (l2 != null) {
				l2 = l2.next;
			}
			if (l1 != null || l2 != null) {
				e.next = new ListNode(0);
				e = e.next;
			}
		}
		if (carry == 1) {
			e.next = new ListNode(0);
			e = e.next;
			e.val = 1;
		}
		return result;
	}
}
