package median;

public class Medium {
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] result = new int[nums1.length + nums2.length];
        int num1 = 0, num2 = 0;
        int length = result.length;
        for (int i = 0; i < result.length; i++) {
        	if (num1 != nums1.length && num2 != nums2.length) {
        		if (nums1[num1] < nums2[num2]) {
        			result[i] = nums1[num1++];
        		}
        		else {
        			result[i] = nums2[num2++];
        		}
        	}
        	else if (num1 != nums1.length && num2 == nums2.length) {
        		result[i] = nums1[num1++];
        	}
        	else if (num1 == nums1.length && num2 != nums2.length) {
        		result[i] = nums2[num2++];
        	}
        }
        double ans;
        if (result.length % 2 != 0) {
        	ans = result[(length-1)/2];
        	return ans;
        }
        else {
        	ans = ((double)result[length/2-1] + (double)result[length/2])/2;
        	return ans;
        }
    }
}
